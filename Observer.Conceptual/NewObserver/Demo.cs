﻿using System;
namespace Observer.Conceptual
{
    public partial class ModernObserver
    {
        public class Demo
        {
            static void Main()
            {
                var person = new Person();

                person.FallsIll += CallDoctor;

                person.CatchACold();
            }

            private static void CallDoctor(object sender, FallsIllEventArgs eventArgs)
            {
                Console.WriteLine($"A doctor has been called to {eventArgs.Address}");
            }
        }
    }
}
