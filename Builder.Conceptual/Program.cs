﻿// Builder Design Pattern
//
// Intent: Lets you construct complex objects step by step. The pattern allows
// you to produce different types and representations of an object using the
// same construction code.

using System;

namespace DesignPatterns.Builder.Conceptual
{

    class Program
    {
        static void Main(string[] args)
        {
            ClassicBuilderExample();

            //ModernBuilderExample();
        }

        static void ClassicBuilderExample()
        {
            // The client code creates a builder object, passes it to the
            // director and then initiates the construction process. The end
            // result is retrieved from the builder object.
            var director = new Director();
            var builder = new ConcreteBuilder();
            director.Builder = builder;

            Console.WriteLine("Standard basic product:");
            director.buildMinimalViableProduct();
            Console.WriteLine(builder.GetProduct().ListParts());

            Console.WriteLine("Standard full featured product:");
            director.buildFullFeaturedProduct();
            Console.WriteLine(builder.GetProduct().ListParts());

            // Remember, the Builder pattern can be used without a Director
            // class.
            Console.WriteLine("Custom product:");
            builder.BuildPartA();
            builder.BuildPartC();
            Console.Write(builder.GetProduct().ListParts());
        }

        static void ModernBuilderExample()
        {
            var personOne = new AddressBuilder()
            .Person
              .Name("Fabrikam")
              .AsA("Engineer")
              .Earning(123000)
            .Lives
              .At("123 London Road")
              .In("London")
              .WithPostcode("SW12BC");

            Console.WriteLine(personOne.build());

        }
    }
}
