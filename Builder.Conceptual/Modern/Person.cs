﻿using System;
public class Person
{
    // address
    public string StreetAddress, Postcode, City;

    // employment
    public string Name, Position, Abilities;

    public int AnnualIncome;

    public override string ToString()
    {
        return $"{nameof(Name)}: {Name}, {nameof(Position)}: {Position}, {nameof(AnnualIncome)}: {AnnualIncome}, {nameof(StreetAddress)}: {StreetAddress}, {nameof(Postcode)}: {Postcode}, {nameof(City)}: {City}";
    }
}
