public class JobBuilder :  Builder
{
    public JobBuilder(Person person)
    {
        this.person = person;
    }

    public JobBuilder Name(string name)
    {
        person.Name = name;
        return this;
    }

    public JobBuilder AsA(string position)
    {
        person.Position = position;
        return this;
    }

    public JobBuilder Earning(int annualIncome)
    {
        person.AnnualIncome = annualIncome;
        return this;
    }
}
