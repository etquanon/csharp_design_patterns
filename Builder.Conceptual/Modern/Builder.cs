﻿public class Builder
// facade 
{
    // the object we're going to build
    protected Person person = new Person(); // this is a reference!

    public AddressBuilder Lives => new AddressBuilder(person);
    public JobBuilder Person => new JobBuilder(person);

    // public static implicit operator Agent(AgentBuilder ag) => ag.agent;
    public Person build() {
        return person;
    }
}
