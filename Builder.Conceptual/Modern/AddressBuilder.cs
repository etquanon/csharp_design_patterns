﻿
public class AddressBuilder : Builder
{
    // might not work with a value type!
    public AddressBuilder(Person person) => this.person = person;

    public AddressBuilder()
    {
    }

    public AddressBuilder At(string streetAddress)
    {
        person.StreetAddress = streetAddress;
        return this;
    }

    public AddressBuilder WithPostcode(string postcode)
    {
        person.Postcode = postcode;
        return this;
    }

    public AddressBuilder In(string city)
    {
        person.City = city;
        return this;
    }

}
